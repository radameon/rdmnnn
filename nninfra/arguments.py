#rdmnNN script arguments helper

import argparse

def parse_arguments(argv):
    parser = argparse.ArgumentParser(description="Simple NN example")
    parser.add_argument("-w", "--weights", required=True, default="weights.txt", help="Path to weights file")
    parser.add_argument("-i", "--input", required=True, default="input.txt", help="File with input data")
    parser.add_argument("-s", "--hidden", required=False, default="hidden.txt", help="File with hidden layer weights")
    parser.add_argument("-n", "--numpy", action="store_true", required=False, default=False, help="Use NumPy for calculations")
    parsed_args = parser.parse_args(argv)

    return parsed_args.weights, parsed_args.input, parsed_args.hidden, parsed_args.numpy