#rdmnNN math helpers

def eltwise_mul(a, b):
    assert(len(a) == len(b))
    ret = list()

    for i in range(len(a)):
        ret.append(a[i] * b[i])

    return ret


def eltwise_add(a, b):
    assert(len(a) == len(b))
    ret = list()

    for i in range(len(a)):
        ret.append(a[i] + b[i])

    return ret


def vector_sum(v):
    output = 0.0
    for i in v:
        output += i

    return output


def vector_avg(v):
    sum = vector_sum(v)
    return sum / len(v)


def weighted_sum(w, i):
    assert(len(w) == len(i))
    output = 0.0

    for j in range(len(w)):
        output += (w[j] * i[j])

    return output

def vec_mat_mul(vec, mat):
    assert(len(vec) == len(mat))
    output = [0, 0, 0]
    for i in range(len(vec)):
        output[i] = weighted_sum(vec, mat[i])
        
    return output

def squared_error(prediction, expected):
    return (prediction - expected) ** 2
