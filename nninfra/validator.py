#rdmnNN validation helpers

import os

def check_input_file(file):
    file = os.path.abspath(file)
    
    if not os.path.exists(file):
        print("Input file doesn't exist!")
        exit(1)
        
def check_weights_file(file):
    file = os.path.abspath(file)
    
    if not os.path.exists(file):
        print("Weights file doesn't exist!")
        exit(1)