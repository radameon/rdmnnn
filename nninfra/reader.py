# rdmnNN helpers

def read_weights(file):
    f = open(file, "r")
    ret = list()
    for l in f:
        l = l.strip()
        ret.append(float(l))

    return ret


def read_inputs(file):
    f = open(file, "r")
    ret = dict()
    for l in f:
        print(l)
        key, values_string = l.split('=')
        key = key.strip()
        ret[key] = list()
        print("Key %s values sgtring %s" % (key, values_string))
        for c in values_string.split(','):
            ret[key].append(float(c))

    f.close()
    return ret


def read_weights_mul(file):
    return read_inputs(file)