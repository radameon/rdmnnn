#!/usr/bin/python3

import sys
import os
import argparse

from nninfra.reader import read_weights, read_inputs
from nninfra.math import weighted_sum

def elementwise_mul(scalar, vector):
    output = [0,0,0]
    assert(len(output) == len(vector))
    
    for i in range(len(vector)):
        output[i] = scalar * vector[i]
        
    return output

def kernel(input, weights):
    return weighted_sum(i=input, w=weights)

def train(training_input, pretrained_weights, actual_data):
    pred = kernel(input=training_input, weights=pretrained_weights)
    error = (pred - actual_data) ** 2
    delta = pred - actual_data
    
    return error, delta

if __name__ == "__main__":
    
    pretrained_weights = [0.1, 0.2, -0.1]
    training_input = read_inputs('input.txt')
    training_data = read_inputs('mul_trn_data.txt')
    
    print(training_input)
    print(training_data)
    
    first_training = [training_input['toes'][0], training_input['wlrec'][0], training_input['nfans'][0]]
    error, delta = train(training_input=first_training, pretrained_weights=pretrained_weights, actual_data=training_data['win_or_lose_binary'][0])
    
    print(f'After first training iteration erros is {error} and delta is {delta}')
    
    weight_deltas = elementwise_mul(delta, first_training)
    
    print(f'Weight deltas {weight_deltas}')
    
    alpha = 0.01
    
    for i in range(len(pretrained_weights)):
        pretrained_weights[i] -= alpha * weight_deltas[i]
        
    print(f'Corrected weights {pretrained_weights}')
    
    print("Done")