#!/usrbin/python3

knob_wieght = 0.5
input = 0.5
goal_pred = 0.8

pred = input * knob_wieght
error = (pred - goal_pred) ** 2

print("Prediction error {}".format(error))