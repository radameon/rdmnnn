#!/usr/bin/python3

import sys
import os
import argparse

from nninfra.reader import read_weights, read_inputs

def parse_arguments(argv):
    parser = argparse.ArgumentParser(description="Simple NN example")
    parser.add_argument("-w", "--weights", required=True, default="weights.txt", help="Path to weights file")
    parser.add_argument("-i", "--input", required=True, default="input.txt", help="File with input data")
    parsed_args = parser.parse_args(argv)

    return parsed_args.weights, parsed_args.input


def eltwise_mul(a, b):
    assert(len(a) == len(b))
    ret = list()

    for i in range(len(a)):
        ret.append(a[i] * b[i])

    return ret

def eltwise_add(a, b):
    assert(len(a) == len(b))
    ret = list()

    for i in range(len(a)):
        ret.append(a[i] + b[i])

    return ret


def vector_sum(v):
    output = 0.0
    for i in v:
        output += i

    return output


def vector_avg(v):
    sum = vector_sum(v)
    return sum / len(v)


def weighted_sum(w, i):
    assert(len(w) == len(i))
    output = 0.0

    for j in range(len(w)):
        output += (w[j] * i[j])

    return output


def kernel(weights, inputs):
    return weighted_sum(weights, inputs)   


def main(weights_file, input_file):
    weights_file = os.path.abspath(weights_file)
    input_file = os.path.abspath(input_file)

    if not os.path.exists(weights_file) or not os.path.exists(input_file):
        print("Weights or input file doesn't exist!")
        exit(1)

    if not os.path.isfile(weights_file) or not os.path.isfile(input_file):
        print("Weights or input path is not a file!")
        exit(2)
    
    inputs = read_inputs(input_file)
    print("Parsed inputs %s" % inputs)
    print("specific val from input %f" % inputs['toes'][0])

    weights = read_weights(weights_file)
    print("Weights %s" % weights)
    print("Specific weights %s" % weights[1])

    predictions_per_game = list()
    for i in range(len(inputs['toes'])):
        prediction = kernel(weights, [inputs['toes'][i], inputs['wlrec'][i], inputs['nfans'][i]])
        print("Inference prediction for game %i: %f" % (i, prediction))
        predictions_per_game.append(prediction)

    print(predictions_per_game)
    return

    
if __name__ == '__main__':
    weights_file, input_file = parse_arguments(sys.argv[1:])

    main(weights_file, input_file)

    print('Inference done')
    exit(0)