#!/usr/bin/python3

import sys
import os
import numpy as np

from nninfra.arguments import parse_arguments
from nninfra.reader import read_inputs, read_weights_mul
from nninfra.validator import check_weights_file, check_input_file
from nninfra.math import vec_mat_mul
from nninfra.weights import remove_labels_from_weights

def kernel(inputs, weights):
    hid = vec_mat_mul(inputs, weights[0])
    pred = vec_mat_mul(hid, weights[1])
    return pred


def numpy_kernel(inputs, weights):
    hid = inputs.dot(weights[0])
    pred = hid.dot(weights[1])
    return pred


def detailed_output(pred):
    print("Injury prob: {}; Win prob: {}; Sadness prob: {};".format(pred[0], pred[1], pred[2]))
    

def main(input_file, hidden_weights_file, weights_file, use_numpy):
    check_weights_file(weights_file)
    check_weights_file(hidden_weights_file)
    check_input_file(input_file)
    
    inputs = read_inputs(input_file)
    weights = read_weights_mul(weights_file)
    hidden_weights = read_weights_mul(hidden_weights_file)
    
    print(inputs)
    print(weights)
    print(hidden_weights)
    
    if not use_numpy:
        weights = remove_labels_from_weights(weights)
        hidden_weights = remove_labels_from_weights(hidden_weights)
        
        print(weights)
        print(hidden_weights)
        aw = [hidden_weights, weights]
        
        predictions_per_game = list()
        for i in range(len(inputs['toes'])):
            prediction = kernel([inputs['toes'][i], inputs['wlrec'][i], inputs['nfans'][i]], aw)
            print("Inference prediction for game {}: {}".format(i, prediction))
            detailed_output(prediction)
            predictions_per_game.append(prediction)

        print(predictions_per_game)
    else:
        ih_wgt = np.array([hidden_weights["hid0"], hidden_weights["hid1"], hidden_weights["hid2"]])
        hp_wgt = np.array([weights["injuries"], weights["victories"], weights["moral"]])
        
        aw = [ih_wgt, hp_wgt]
        
        predictions_per_game = list()
        for i in range(len(inputs['toes'])):
            prediction = kernel(np.array([inputs['toes'][i], inputs['wlrec'][i], inputs['nfans'][i]]), aw)
            print("Inference prediction for game {}: {}".format(i, prediction))
            detailed_output(prediction)
            predictions_per_game.append(prediction)

        print(predictions_per_game)
    

if __name__ == '__main__':
    weights_file, input_file, hidden_weights_file, use_numpy = parse_arguments(sys.argv[1:])
    
    main(input_file, hidden_weights_file, weights_file, use_numpy)
    
    print("Inference done!")
    exit(0)