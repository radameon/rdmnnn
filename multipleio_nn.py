#!/usr/bin/python3

import sys
import os
import argparse

from nninfra.reader import read_weights_mul, read_inputs
from nninfra.arguments import parse_arguments
from nninfra.validator import check_input_file, check_weights_file
from nninfra.math import vec_mat_mul

def kernel(weights, input):
    pred = vec_mat_mul(input, weights)
    return pred

def detailed_output(pred):
    print("Injury prob: {}; Win prob: {}; Sadness prob: {};".format(pred[0], pred[1], pred[2]))

def main(weights_file, input_file):
    check_weights_file(weights_file)
    check_input_file(input_file)
    
    weights = read_weights_mul(weights_file)
    inputs = read_inputs(input_file)
    
    print(weights)
    print(inputs)   
    
    predictions_per_game = list()
    for i in range(len(inputs['toes'])):
        prediction = kernel([weights["injuries"], weights["victories"], weights["moral"]], [inputs['toes'][i], inputs['wlrec'][i], inputs['nfans'][i]])
        print("Inference prediction for game {}: {}".format(i, prediction))
        detailed_output(prediction)
        predictions_per_game.append(prediction)

    print(predictions_per_game)
    

if __name__ == '__main__':
    weightsf, inputsf = parse_arguments(sys.argv[1:])
    
    main(weightsf, inputsf)
    
    print('Inference done')
    exit(0)