#!/usr/bin/python3

def kernel(input, weight):
    return input * weight


def train_iter(weight, input, goal_prediction, step):
    pred = kernel(input, weight)
    error = (pred - goal_prediction) ** 2
    
    print("Error: {}\tPrediction: {}".format(error, pred))
    
    up_pred = kernel(input, weight + step)
    up_err = (up_pred - goal_prediction) ** 2
    
    dn_pred = kernel(input, weight - step)
    dn_err = (dn_pred - goal_prediction) ** 2
    
    if dn_err < up_err:
        weight -= step
        
    if up_err < dn_err:
        weight += step
        
    return weight

def trivial_gradient_descent(weight, input, goal_prediction):
    pred = kernel(input, weight)
    error = (pred - goal_prediction) ** 2
    
    direction_and_amount = (pred - goal_prediction) * input
    weight  = weight - direction_and_amount
    
    print("Error: {}\tPrediction: {}".format(error, pred))
    
    return weight


def delta_gradient_descent(weight, input, goal_prediction):
    print("--------\nWeight:{}".format(weight))
    alpha = 1.001
    pred = kernel(input, weight)
    error = (pred - goal_prediction) ** 2
    
    delta = pred - goal_prediction
    weight_delta = input * delta
    
    weight = weight - (weight_delta * alpha)
    print("Error:{}\tPrediction:{}".format(error, pred))
    print("Weight:{}\tWeight_delta:{}".format(weight, weight_delta))
    
    return weight    


def singular_example():
    weights = 0.1
    lr = 0.01
    
    number_of_toes = [8.5]
    win_or_lose_binary = [1]
    
    inputs = number_of_toes[0]
    ground_truth = win_or_lose_binary[0]
    
    pred = kernel(inputs, weights)
    
    error = (pred - ground_truth) ** 2
    print("Quadratic error {}".format(error))
    
    print("Up weight")
    
    p_up = kernel(inputs, weights + lr)
    
    uerror = (p_up - ground_truth) ** 2
    print("Quadratic error after weight increase {}".format(uerror))
    
    print("Down weight")
    p_d = kernel(inputs, weights - lr)
    
    derror = (p_d - ground_truth) ** 2
    print("Quadratic error after weight decrease {}".format(derror))
    
    if error > uerror or error > derror:
        if uerror < derror:
            weights += lr
            
        if derror < uerror:
            weights -= lr
            
    print("New weight {}". format(weights))


def main():
    weight = 0.5
    initial_weight = weight
    inputs = 0.5
    goal_prediction = 0.8
    step = 0.001
    
    for i in range(1101):
        weight = train_iter(weight, inputs, goal_prediction, step)
        
    print("Initial weight: {}\t Resulting weight: {}".format(initial_weight, weight))
    
    weight = initial_weight
    
    for i in range(20):
        weight = trivial_gradient_descent(weight, inputs, goal_prediction)
    
    print("Initial weight: {}\t Resulting weight: {}".format(initial_weight, weight))
    
    weight, goal_prediction, input = (0.0, 0.8, 1.1)
    for i in range(4):
        weight = delta_gradient_descent(weight, input, goal_prediction)
    
    print("Initial weight: {}\t Resulting weight: {}".format(initial_weight, weight))
    


if __name__ == '__main__':
    main()
    
    print("Training done")